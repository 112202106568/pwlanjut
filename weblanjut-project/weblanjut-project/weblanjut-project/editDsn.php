<!DOCTYPE html>
<html>
<head>
	<title>Sistem Informasi Akademik::Edit Data Dosen</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="bootstrap4/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/styleku.css">
	<script src="bootstrap4/jquery/3.3.1/jquery-3.3.1.js"></script>
	<script src="bootstrap4/js/bootstrap.js"></script>
</head>
<body>
	<?php
	session_start();
	if (!isset($_SESSION['username'])) {
		header("Location:index.php");
	}
	require "fungsi.php";
	require "head.html";
	$npp=$_GET["kode"];
	$sql="select * from dosen where npp='$npp'";
	$qry=mysqli_query($koneksi,$sql);
	$row=mysqli_fetch_assoc($qry);
	function getHomeBase($homebase, $field){
		$hasil = ($homebase == $field) ? "selected" : "";
		return $hasil;
	}
	?>
	<div class="utama">
		<h2 class="mb-3 text-center">EDIT DATA DOSEN</h2>	
		<div class="row">
		<div class="col-sm-9">
			<form enctype="multipart/form-data" method="post" action="sv_editDsn.php">
				<div class="form-group">
					<label for="npp">NPP:</label>
					<input class="form-control" type="text" name="npp" id="npp" value="<?php echo $row['npp']?>" required readonly>
				</div>
				<div class="form-group">
					<label for="nama">Nama:</label>
					<input class="form-control" type="text" name="nama" id="nama" required value="<?php echo $row['namadosen']?>">
				</div>
				<div class="form-group">
					<label for="homebase">Homebase:</label>
					<select name="homebase" id="homebase" class="custom-select custom-select-lg">
						<option value="A11" <?= getHomeBase($row["homebase"], "A11")?>>A11 - Teknik Informatika</option>
						<option value="A12" <?= getHomeBase($row["homebase"], "A12")?>>A12 - Sistem Informasi</option>
						<option value="A14" <?= getHomeBase($row["homebase"], "A14")?>>A14 - Desain Komunikasi Visual</option>
						<option value="A15" <?= getHomeBase($row["homebase"], "A15")?>>A15 - Ilmu Komunikasi</option>
						<option value="A22" <?= getHomeBase($row["homebase"], "A22")?>>A22 - Teknik Informatika (D3)</option>
					</select>
				</div>				
				<div>		
					<button class="btn btn-primary" type="submit" id="submit">Simpan</button>
				</div>
			</form>
		</div>
		</div>
	</div>
	<script>
		// $('#submit').on('click',function(){
		// 	var id 		= $('#id').val();
		// 	var nama	= $('#nama').val();
		// 	var email 	= $('#email').val();
		// 	$.ajax({
		// 		method	: "POST",
		// 		url		: "sv_editMhs.php",
		// 		data	: {id:id, nama:nama, email:email}
		// 	});
		// });
	</script>
</body>
</html>