<?php
session_start();
if (!isset($_SESSION['username'])) {
	header("Location:index.php");
}
require "fungsi.php";
$hasil = mysqli_query($koneksi, "SELECT npp FROM dosen");
// $data = [];
$i = 0;
while($row = mysqli_fetch_row($hasil)){

	$data[$i] = str_replace(".", "", substr($row[0], 13, 3) );
	$i++;
}
$id_npp;
while(true){
	$id_npp = rand(100, 999);
	if (!in_array($id_npp, $data)) {
		break;
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Sistem Informasi Akademik::Tambah Data Dosen</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="bootstrap4/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/styleku.css">
	<script src="bootstrap4/jquery/3.3.1/jquery-3.3.1.js"></script>
	<script src="bootstrap4/js/bootstrap.js"></script>

</head>
<body>
	<?php
		require "head.html";
	?>
	<div class="utama">		
		<br><br><br>		
		<h3>TAMBAH DATA DOSEN</h3>
		<div class="alert alert-success alert-dismissible" id="success" style="display:none;">
	  		<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
		</div>	
		<form method="post" action="sv_addDsn.php" enctype="multipart/form-data">
			<div class="form-grou npp">
				<label for="npp">NPP:</label>
				<div class="col-sm-10 offset-sm-1">
				<div class="form-group">
					<label for="tahun">Tahun:</label>
					<select name="tahun" id="tahun" class="custom-select">
						<?php for ($i=1970; $i <= 2050; $i++) { ?>
							<option value="<?=$i?>"><?=$i?></option>
							<?php
								if ($i == 2023) {?>
									<option value="<?=$i?>" selected><?=$i?></option>
								<?php }
							?>
						<?php } ?>
					</select>
				</div>
				<input class="form-control" type="text" name="npp" id="npp" value="0686.11.2023.<?=$id_npp?>">
				</div>
			</div>
			<div class="form-group">
				<label for="nama">Nama:</label>
				<input class="form-control" type="text" name="nama" id="nama" required>
			</div>
			<div class="form-group">
				<label for="homebase">Homebase:</label>
				<select name="homebase" id="homebase" class="custom-select custom-select-lg">
						<option value="A11">A11 - Teknik Informatika</option>
						<option value="A12">A12 - Sistem Informasi</option>
						<option value="A14">A14 - Desain Komunikasi Visual</option>
						<option value="A15">A15 - Ilmu Komunikasi</option>
						<option value="A22">A22 - Teknik Informatika (D3)</option>
					</select>
			</div>
			<div>		
				<button type="submit" class="btn btn-primary" value="Simpan">Simpan</button>
			</div>
		</form>
	</div>
	<script>
		npp = document.getElementById('npp');
		tahun = document.getElementById('tahun');
		tahun.addEventListener("change", function () {
			value = npp.value.slice(8, 12);
			npp.value = npp.value.replace(value, tahun.value)

		});
	</script>
	<!--
	<script>
		$(document).ready(function(){
			$('#butsave').on('click',function(){			
				$('#butsave').attr('disabled', 'disabled');
				var nim 	= $('#nim').val();
				var nama	= $('#nama').val();
				var email 	= $('#email').val();
				
				$.ajax({
					type	: "POST",
					url		: "sv_addMhs.php",
					data	: {
								nim:nim,
								nama:nama,
								email:email
							  },
					contentType	:"undefined",					
					success : function(dataResult){						
						alert('success');
						$("#butsave").removeAttr("disabled");
						$('#fupForm').find('input:text').val('');
						$("#success").show();
						$('#success').html(dataResult);	
					}	   
				});
			});
		});
	</script>
	-->	
</body>
</html>